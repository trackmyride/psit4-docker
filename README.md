Inside the local repository clone run:

```
sudo docker login registry.gitlab.com
sudo docker build -t registry.gitlab.com/trackmyride/psit4-docker:8.9.4 .
sudo docker push registry.gitlab.com/trackmyride/psit4-docker:8.9.4
```